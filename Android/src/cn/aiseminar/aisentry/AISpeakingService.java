package cn.aiseminar.aisentry;

import cn.aiseminar.aisentry.aimouth.AIMouth;
import cn.aiseminar.aisentry.transceiver.Transceiver.DataTransferThread;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

public class AISpeakingService extends Service {
	
	private AIMouth mMouth = null;
	private AISMouthHandler mMouthHandler = null;
	private AISpeakingBinder mSpeakingBinder = null;
	
	public Handler getMouthHandler() {
		return mMouthHandler;
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public void onCreate() {
		super.onCreate();
		
		mMouthHandler = new AISMouthHandler();
		mSpeakingBinder = new AISpeakingBinder();
		listenClipborad();
		
		startSpeakingForeground();
	}
	
	@SuppressLint("NewApi")
	private void listenClipborad()
	{
		ClipboardManager cbManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
		cbManager.setPrimaryClip(ClipData.newPlainText("", ""));
		cbManager.addPrimaryClipChangedListener(new OnPrimaryClipChangedListener() {
			
			@Override
			public void onPrimaryClipChanged() {
				ClipboardManager cbManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
				String text = String.valueOf(cbManager.getPrimaryClip().getItemAt(0).getText());
				mMouth.speak(text);
			}
		});
	}

	@Override
	public IBinder onBind(Intent arg0) {
		if (null == mMouth)
		{
			mMouth = AIMouth.getMouth(this);
		}
		mMouth.setMsgHandler(mMouthHandler);
		
		return mSpeakingBinder;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (null == mMouth)
		{
			mMouth = AIMouth.getMouth(this);
		}
		mMouth.setMsgHandler(mMouthHandler);
		
		flags = START_STICKY;
		
		return super.onStartCommand(intent, flags, startId);
	}

	public void resetMouthHandler()
	{
		if (null != mMouth)
		{
			mMouth.setMsgHandler(mMouthHandler);
		}
	}
	
	public class AISpeakingBinder extends Binder
	{
		public AISpeakingService getSpeakingService()
		{
			return AISpeakingService.this;
		}
	}
	
	@SuppressLint("HandlerLeak")
	class AISMouthHandler extends Handler
    {
		@Override
		public void handleMessage(Message msg) {
			if (AISMessageCode.MOUTH_MSG_BASE + AIMouth.TTS_State.TTS_READY.ordinal() == msg.what)
			{
				if (null != mMouth)
				{
					int iSpeed = getSharedPreferences(AISpeechSetting.SP_SPEECH_SETTING, MODE_PRIVATE).getInt(AISpeechSetting.SP_SPEED_KEY, AISpeechSetting.SPEECH_SPEED_DEFAULT);
					mMouth.setSpeechSpeed(iSpeed);
					mMouth.speak(AISpeakingService.this.getString(R.string.st_welcome));
				}
				return;
			}
			
			if (AISMessageCode.TRANSCEIVER_MSG_BASE + DataTransferThread.DATATYPE_PLAINTEXT == msg.what)
			{
				if (null != mMouth && null != msg.obj)
				{
					String message = (String) msg.obj;
					mMouth.speak(message);
				}
				return;
			}
			
			super.handleMessage(msg);
		}
    }

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	@SuppressLint("NewApi") 
	public void startSpeakingForeground() {
		Notification.Builder builder = new Notification.Builder(this);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, 
				new Intent(this, AISEntryActivity.class), 0);
		builder.setContentIntent(contentIntent);
		builder.setSmallIcon(R.drawable.ic_launcher);
		builder.setTicker(getResources().getText(R.string.app_name));
		builder.setContentTitle(getResources().getText(R.string.app_name));
		builder.setContentText(getResources().getText(R.string.msg_speaking_service_running));
		
		Notification notification = builder.build();
		
		startForeground(1, notification);
	}
}
