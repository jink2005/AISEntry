package cn.aiseminar.aisentry;

import java.util.ArrayList;
import java.util.List;

import org.cybergarage.upnp.ControlPoint;
import org.cybergarage.upnp.Device;
import org.cybergarage.upnp.DeviceList;
import org.cybergarage.upnp.device.DeviceChangeListener;
import org.cybergarage.upnp.device.NotifyListener;
import org.cybergarage.upnp.device.SearchResponseListener;
import org.cybergarage.upnp.event.EventListener;
import org.cybergarage.upnp.ssdp.SSDPPacket;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class AISDeviceControl extends Activity implements NotifyListener, EventListener, SearchResponseListener, DeviceChangeListener {
	
	public static final int MSG_NOTIFY_DEV_ADDED = 0;
	public static final int MSG_NOTIFY_DEV_REMOVED = 1;
	
	private ControlPoint mCtrlPoint = null;
	private ListView mDevListView = null;
	private ArrayAdapter<String> mDevListAdapter = null;
	private ArrayList<String> mDevList = null;
	DeviceList mUpnpDevList = null;
	
	private Handler mDevHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg) {
			mDevListAdapter.notifyDataSetChanged();
			super.handleMessage(msg);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.layout_device_list);
		
		mDevListView = (ListView) this.findViewById(R.id.upnpDevList);
		mDevList = new ArrayList<String>();
		mDevListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, mDevList);
		mDevListView.setAdapter(mDevListAdapter);
		
		mCtrlPoint = new ControlPoint();
		mCtrlPoint.addNotifyListener(this);
		mCtrlPoint.addEventListener(this);
		mCtrlPoint.addSearchResponseListener(this);
		mCtrlPoint.addDeviceChangeListener(this);
		Thread startThread = new Thread() {
			@Override
			public void run() {
				mCtrlPoint.start();
				mCtrlPoint.search("upnp:rootdevice");
			}
		};
		startThread.start();
	}
	
	@Override
	protected void onStart() {
		this.syncDeviceList();
		mDevListAdapter.notifyDataSetChanged();
		super.onStart();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mCtrlPoint.stop();
		super.onDestroy();
	}

	@Override
	public void deviceNotifyReceived(SSDPPacket ssdpPacket) {
		// TODO Auto-generated method stub
		System.out.println(ssdpPacket.toString());
		
		if (ssdpPacket.isDiscover() == true) {
			String st = ssdpPacket.getST();
			System.out.println("ssdp:discover : ST = " + st);
		}
		else if (ssdpPacket.isAlive() == true) {
			String usn = ssdpPacket.getUSN();
			String nt = ssdpPacket.getNT();
			String url = ssdpPacket.getLocation();
			System.out.println("ssdp:alive : uuid = " + usn + ", NT = " + nt + ", location = " + url); 
		}
		else if (ssdpPacket.isByeBye() == true) {
			String usn = ssdpPacket.getUSN();
			String nt = ssdpPacket.getNT();
			System.out.println("ssdp:byebye : uuid = " + usn + ", NT = " + nt); 
		}
	}

	@Override
	public void eventNotifyReceived(String uuid, long seq, String varName,
			String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deviceSearchResponseReceived(SSDPPacket ssdpPacket) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deviceAdded(Device dev) {
		this.syncDeviceList();
		Message msg = new Message();
		msg.what = MSG_NOTIFY_DEV_ADDED;
		mDevHandler.sendMessage(msg);
	}

	@Override
	public void deviceRemoved(Device dev) {
		this.syncDeviceList();
		Message msg = new Message();
		msg.what = MSG_NOTIFY_DEV_ADDED;
		mDevHandler.sendMessage(msg);
	}
	
	public void syncDeviceList() {
		mUpnpDevList = mCtrlPoint.getDeviceList();
		
		mDevList.clear();
		for (int i = 0; i < mUpnpDevList.size(); i ++) {
			Device dev = mUpnpDevList.getDevice(i);
			mDevList.add(dev.getFriendlyName());
		}
	}

}
