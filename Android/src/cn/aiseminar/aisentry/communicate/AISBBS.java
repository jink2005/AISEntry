package cn.aiseminar.aisentry.communicate;

import cn.aiseminar.aisentry.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("SetJavaScriptEnabled") public class AISBBS extends Activity {
	private WebView mWebView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); // hidden title  
		this.setContentView(R.layout.activity_bbs);
		mWebView = (WebView) this.findViewById(R.id.webBBS);
		WebSettings webSettings = mWebView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		mWebView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// TODO Auto-generated method stub
				
				return super.shouldOverrideUrlLoading(view, url);
			}
			
		});
		mWebView.loadUrl("http://www.aiseminar.cn/bbs/forum.php?mobile=2");
	}
}
