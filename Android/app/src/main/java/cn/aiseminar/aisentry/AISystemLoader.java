package cn.aiseminar.aisentry;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;

public class AISystemLoader {
    public static final String mAppId = "5350daf2";
	private Context mContext = null;
	
	public AISystemLoader(Context context)
	{
		mContext = context;
	}

	public void checkEnvironment()
	{
        SpeechUtility.createUtility(mContext, SpeechConstant.APPID + "=" + mAppId);
		if (! SpeechUtility.getUtility().checkServiceInstalled()) {
			installSpeechServiceDialog();
		}
	}
	
	public void installSpeechServiceDialog()
	{
		AlertDialog.Builder dgBuilder = new AlertDialog.Builder(mContext);
		dgBuilder.setMessage(R.string.msg_goto_iflytek);
		dgBuilder.setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String url = SpeechUtility.getUtility().getComponentUrl();
				Uri uri = Uri.parse(url);
				Intent it = new Intent(Intent.ACTION_VIEW, uri);
				mContext.startActivity(it);
			}
		});
		dgBuilder.setNegativeButton(android.R.string.cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dgBuilder.create().show();
	}
}
