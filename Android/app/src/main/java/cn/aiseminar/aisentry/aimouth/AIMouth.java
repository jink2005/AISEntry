package cn.aiseminar.aisentry.aimouth;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;

import cn.aiseminar.aisentry.AISMessageCode;

public class AIMouth extends Object {
	public static enum TTS_State
	{
		TTS_INITING,
		TTS_READY,
		TTS_ERROR,
		TTS_SPEAK_COMPLETED,
	};
	
	private Context mContext = null;
	private SpeechSynthesizer mSpeechSynthesizer = null;
	private TTS_State mSynthesizerState = TTS_State.TTS_INITING;
	
	private Handler mMsgHandler = null;
	
	private static AIMouth gEntryMouth = null;
	public static AIMouth getMouth(Context context)
	{
		if (null == gEntryMouth && null != context)
		{
			gEntryMouth = new AIMouth(context);
		}
		return gEntryMouth;
	}
	
	private AIMouth(Context context)
	{
		mContext = context;
		mSpeechSynthesizer = SpeechSynthesizer.createSynthesizer(mContext, new InitListener() {
			@Override
			public void onInit(int i) {
				if (ErrorCode.SUCCESS == i) {
					mSynthesizerState = TTS_State.TTS_READY;
					Message mess = new Message();
					mess.what = AISMessageCode.MOUTH_MSG_BASE + mSynthesizerState.ordinal();
					mMsgHandler.sendMessage(mess);
				}
			}
		});
		setVoiceReference();
	}
	
	public Handler getMsgHandler() {
		return mMsgHandler;
	}

	public void setMsgHandler(Handler mMsgHandler) {
		this.mMsgHandler = mMsgHandler;
	}
		
	@Override
	protected void finalize() throws Throwable {
		mSpeechSynthesizer.stopSpeaking();
		mSpeechSynthesizer.destroy();
		super.finalize();
	}

	private void setVoiceReference()
	{
		mSpeechSynthesizer.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_LOCAL);
		mSpeechSynthesizer.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
		mSpeechSynthesizer.setParameter(SpeechConstant.PITCH, "50");
		mSpeechSynthesizer.setParameter(SpeechConstant.VOLUME, "100");
	}
	
	public void setSpeechSpeed(int speechSpeed)
	{
		mSpeechSynthesizer.setParameter(SpeechConstant.SPEED, String.valueOf(speechSpeed));
	}
	
	public void speak(String sentence)
	{
		mSpeechSynthesizer.stopSpeaking();
		mSpeechSynthesizer.startSpeaking(sentence, mSynthesizerListener);
	}
	
	public void stop()
	{
		mSpeechSynthesizer.stopSpeaking();
	}

	private SynthesizerListener mSynthesizerListener = new SynthesizerListener() {

		@Override
		public void onSpeakBegin() {

		}

		@Override
		public void onBufferProgress(int i, int i1, int i2, String s) {

		}

		@Override
		public void onSpeakPaused() {

		}

		@Override
		public void onSpeakResumed() {

		}

		@Override
		public void onSpeakProgress(int i, int i1, int i2) {

		}

		@Override
		public void onCompleted(SpeechError speechError) {
			Message mess = new Message();
			mess.what = AISMessageCode.MOUTH_MSG_BASE + TTS_State.TTS_SPEAK_COMPLETED.ordinal();
			mMsgHandler.sendMessage(mess);
		}

		@Override
		public void onEvent(int i, int i1, int i2, Bundle bundle) {

		}
	};
}
