#AIEntry

应用介绍:
=========
AISEntry的目标是建立一个自然沟通方式的Android程序，目标是实现人工智能在产品上的集成。目前先以服务用户，方便使用为目的，利用讯飞语音+，实现使用Android端程序朗读文本文件、朗读其他程序中复制文本，并且与PC端搭配朗读PC端选中文本（如短信、网页文字）的功能。方便用户在手机和PC上听新闻、听文章。

功能介绍：
=========
1. 支持TXT文本的管理和朗读； 
2. 在android3.0以上系统，支持朗读其他应用中复制的文本（选中文本点击复制，立即朗读）； 
3. 支持与PC端连接，并朗读PC端选中的文本。 
PC端下载地址：http://pan.baidu.com/s/1c0IjEhU 
4. 支持语音朗读速度设置。
5. 加入DLAN&UPnP支持，实现媒体服务器、媒体播放器及媒体控制器功能。

引用开源项目：
=============
Circular layout for android
<a href="https://github.com/dmitry-zaitsev/CircleLayout">https://github.com/dmitry-zaitsev/CircleLayout</a>
CyberLink4Java
<a href="https://github.com/cybergarage/CyberLink4Java">https://github.com/cybergarage/CyberLink4Java</a>

更新记录：
=========
2014-06-11
支持语速设置、复制任意文本自动朗读功能。
2014-05-21
实现PC端与Android端的网络互联，通过PC端选中文本点击操作，实现Android端的文本朗读。